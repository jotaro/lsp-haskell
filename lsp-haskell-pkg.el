;;; -*- no-byte-compile: t -*-
(define-package "lsp-haskell" "20191230.1847" "Haskell support for lsp-mode" '((lsp-mode "3.0")) :keywords '("haskell"))
